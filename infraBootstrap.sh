apt-get -y update
apt-get -y upgrade
apt-get -y install nginx
apt-get -y install default-mysql-client
apt-get -y install nodejs
apt-get -y install npm
apt-get -y install python3-dev
apt-get -y install python3-pip