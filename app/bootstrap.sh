npm install
npm run build
mkdir -p /var/www/citefix
cp -r build/* /var/www/citefix
cp nginx.default.conf /etc/nginx/sites-available/citefix
ln -s /etc/nginx/sites-available/citefix /etc/nginx/sites-enabled/
rm /etc/nginx/sites-enabled/default
systemctl restart nginx