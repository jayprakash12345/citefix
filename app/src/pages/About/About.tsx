import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";

function About() {
  return (
    <Container fixed>
      <Paper elevation={6} sx={{ marginTop: "80px", padding: "30px" }}>
        <Typography variant="h4">CiteFix: a tool to fix broken references</Typography>
        <br/>
        <Typography variant="subtitle2">The tool will scan through the articles in the defined
          categories, and allows the user to take action by suggesting appropriate edits in
          fixing the error. The user interface will display a preview along with the edit space
          for both the current version and the suggested edit version. If the user feels the
          suggested edit is appropriate, then the user can approve the suggestion and fix the
          error with a click.
        </Typography>
        <br/>
        <br/>
        <Typography variant="h5">Author:</Typography>
        <ul>
          <li>
            <a href="https://meta.wikimedia.org/wiki/User:Jayprakash12345">Jay Prakash</a>
            <span> ( Developer ) </span>
          </li>
          <li>
            <a href="https://meta.wikimedia.org/wiki/User:Nivas10798">Nivas</a>
            <span> ( Product Manager ) </span>
          </li>
        </ul>
        <br/>
        <Typography variant="h5">Credit:</Typography>
        <ul>
          <li>
            <a href="https://meta.wikimedia.org/wiki/WikiCred">WikiCred</a>
            <span> for funding the development of tool</span>
          </li>
          <li>
            <a href="https://en.wikipedia.org/wiki/User:Cacycle/diff">User:Cacycle</a>
            <span> for wikEd diff JavaScript engine</span>
          </li>
        </ul>
      </Paper>
    </Container>
  )
}

export default About;