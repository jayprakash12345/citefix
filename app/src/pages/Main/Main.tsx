import { ChangeEvent, useState } from "react";

import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import { IUserState } from "../../redux/user/userSlice";
import { useAppSelector } from "../../redux/hooks";
import { RootState } from "../../redux/store";

import { Link, useNavigate, useSearchParams } from "react-router-dom";
import backendApi from "../../config/backendApi";


function Main() {
  const navigate = useNavigate();
  let [params, setParams] = useSearchParams();
  const { username } = useAppSelector<IUserState>((state: RootState) => state.user);

  const [language, setLanguage] = useState<string>(params.get("language") || "en");
  const [category, setCategory] = useState<string>(params.get("category") || "cs1");
  const [page, setPage] = useState<string>(params.get("page") || "");

  const handlePageChange = (e: ChangeEvent<HTMLInputElement>) => {
    setPage(e.target.value);
  }

  const handleLoadClick = () => {
    backendApi.post("/fix", {
        language: language,
        category: category,
        page: page
    }, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(({ data }) => {
      navigate("/fix/" + data["id"]);
    });
  }

  return (
    <Container fixed>
      <Paper elevation={6} sx={{ marginTop: "80px", padding: "30px" }}>
        {username !== "" && username !== null ?
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <InputLabel>Language</InputLabel>
                <Select value={language} defaultValue="en" label="Language" disabled>
                  <MenuItem value="en">English</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6} sx={{ marginTop: "20px" }}>
              <FormControl fullWidth>
                <InputLabel>Category</InputLabel>
                <Select value={category} defaultValue="cs1" label="Language" disabled>
                  <MenuItem value="cs1">CS1 errors: URL</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6} sx={{ marginTop: "20px" }}>
              <TextField value={page} label="Page name" variant="outlined" 
                onChange={handlePageChange} fullWidth
              />
            </Grid>
            <Grid item xs={6}></Grid>
            <Button
              variant="outlined" size="large"
              sx={{ marginLeft: "16px", marginTop: "20px" }}
              onClick={handleLoadClick}
            >Load</Button>
          </Grid>
          :
          <Grid container spacing={2}>
            <Grid container justifyContent="center">
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/d/d1/CiteFix_logo.svg"
                alt="CiteFix logo"
              />
            </Grid>
            <Grid container justifyContent="center" sx={{ marginTop: "50px" }}>
              <Typography variant="h6">
                Please <a href="/api/login">login</a> first to use the tool.
              </Typography>
            </Grid>
          </Grid>
        }
      </Paper>
    </Container>
  )
}

export default Main;