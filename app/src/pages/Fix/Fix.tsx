import Container from "@mui/material/Container";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import backendApi from "../../config/backendApi";
import axios from "axios";

/* eslint-disable no-use-before-define */
import WikEdDiff from "./diff";

function Fix() {
    const wikEdDiff = new WikEdDiff();
    const { uuId } = useParams();
    /* eslint-ignore */
    const [ suggestedContent, setSuggestedContent ] = useState<string>("");
    const [ diff, setDiff ] = useState<string>("");

    useEffect( ()=> {
        backendApi.post("/newcontent/" + uuId)
            .then( ( { data } )=> {
                const tempContent = data["revision"]
                setSuggestedContent(tempContent);

                const PARAMS = {
                    "action": "parse",
                    "format": "json",
                    "page": data["metadata"]["page"],
                    "prop": "wikitext",
                    "formatversion": "2",
                    "origin": "*"
                };
        
                axios.get(`https://${data["metadata"]["language"]}.wikipedia.org/w/api.php`, {
                    params: PARAMS
                }).then( ( { data })=> {
                    setDiff( wikEdDiff.diff( data["parse"]["wikitext"], tempContent ) );
                })

                
            })
    }, [])
    return (
        <Container fixed>
            { diff !== null && diff !== "" ?
                <div style={{marginTop: "80px"}} dangerouslySetInnerHTML={{__html: diff}}></div>
                : null
            }
            {/* TODO: Implement the form with edit area and button */ }
        </Container>
    )
}

export default Fix;