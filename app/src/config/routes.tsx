import { createBrowserRouter } from "react-router-dom";
import PageLayout from "../components/PageLayout";
import Main from "../pages/Main";
import About from "../pages/About";
import Fix from "../pages/Fix";

const router = createBrowserRouter([
  {
    element: <PageLayout />,
    children: [
      {
        path: "/",
        element: <Main />,
      },
      {
        path: "/about",
        element: <About />,
      },
      {
        path: "/fix/:uuId",
        element: <Fix />,
      }
    ]
  }
]);

export default router;