import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import backendApi from '../../config/backendApi';

export interface IUserState {
  username: string | null,
  logged: boolean,
  loading: boolean
}

interface IUserAPIResp {
  username: string,
  logged: boolean
}

const initialState: IUserState = {
  username: null,
  logged: false,
  loading: false
}

const fetchUser = createAsyncThunk(
  'user/fetch',
  async () => {
    const response = await backendApi.get("/profile");
    const data = await response.data;
    return data
  }
)

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Pending
    builder.addCase(fetchUser.pending, (state: IUserState) => {
      state.loading = true
    })

    // Request fulfilled
    builder.addCase(fetchUser.fulfilled, (state: IUserState, 
        action: PayloadAction<IUserAPIResp>
      ) => {
      state.username = action.payload.username;
      state.logged = action.payload.logged;
      state.loading = false;
    })

    // Request reject
    builder.addCase(fetchUser.rejected, (state: IUserState, action) => {
      state.loading = false;
    })
  },
})

export { fetchUser };
export default userSlice.reducer;