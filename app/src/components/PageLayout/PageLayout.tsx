import { Outlet } from "react-router-dom";
import Header from "../Header";
import CssBaseline from "@mui/material/CssBaseline";

const PageLayout = () => (
  <>
    <CssBaseline />
    <Header />
    <Outlet />
  </>
);

export default PageLayout;