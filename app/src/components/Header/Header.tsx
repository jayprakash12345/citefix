import React, { useEffect } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { IUserState, fetchUser } from '../../redux/user/userSlice';
import { RootState } from '../../redux/store';


function Header() {
  const dispatch = useAppDispatch();
  const { username } = useAppSelector<IUserState>( (state: RootState) => state.user );

  useEffect( () => {
    dispatch( fetchUser() );
  }, [ dispatch ]);

  return (
    <AppBar position="static" sx={{ bgcolor: "#9c27b0" }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography variant="h6" noWrap component="a" href="/"
            sx={{
              mr: 2,
              display: 'flex',
              fontFamily: 'monospace',
              fontWeight: 700,
              fontSize: '1.5rem',
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/d/d1/CiteFix_logo.svg"
              height="40px"
              alt="CiteFix logo"
            />
            <span style={{ marginTop: '-2px' }}>Fix</span>
          </Typography>
          <Box sx={{ flexGrow: 1, display: 'flex' }}>
            <Button component={Link} to="/" sx={{ my: 2, color: 'white' }}>
              Home
            </Button>
            <Button component={Link} to="/about" sx={{ my: 2, color: 'white' }}>
              About
            </Button>
          </Box>
          {username !== "" && username !== null ? 
            <>
              <Typography sx={{mr: 2}}>Logged in as {username}</Typography>
              <Button variant="contained" href="/api/logout" color="secondary" style={{ backgroundColor: 'red', color: 'black', fontWeight: 'bolder' }}>
                Logout
              </Button>
            </>
            :
            <Button variant="contained" href="/api/login" style={{ backgroundColor: 'orange', color: 'black', fontWeight: 'bolder' }}>
              Login
            </Button>
          }
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default Header;
