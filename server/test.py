from utils.api import WikiAPI
from utils.fixes import Fixes
from utils.url import isValidURL
from utils.wiki import WikiPage

from mwparserfromhell import parse
import re

# apiObj = WikiAPI()
# pageObj = WikiPage("Alex Smith")
# pageObj.setup(apiObj)

# fixes = Fixes(pageObj)
wikiCode = parse("""
{{Short description|Iranian photojournalist}}
{{Infobox person
| name               = Ali Kaveh
| native_name        = علی کاوه
| image              = Ali Kaveh 2019-08-07.jpg
| caption            = 
| birthname          = 
| birth_date         = 1946
| birth_place        = [[Amol]], [[Iran]]
| death_date         = 
| death_place        = 
| education          = 
| occupation         = Photographer
| alias              = 
| title              = 
| parents            = 
| family             = 
| spouse             = 
| domestic_partner   = 
| children           = 
| relatives          = 
| credits            = 
| URL                = 
}}

'''Ali Kaveh''' (born 1946 in [[Amol]]) is an Iranian photojournalist.<ref>{{Cite web|last=Behnegarsoft.com|date=2013-07-15|title=زندگینامه علی کاوه پیش از انتشار خواندنی شد {{!}} ایبنا|url=http://www.ibna.ir/fa/tolidi/174010/زندگینامه-علی-کاوه-پیش-انتشار-خواندنی|access-date=2020-11-01|website=خبرگزاری کتاب ايران (IBNA)|language=fa}}</ref><ref>{{Cite web|date=2007-10-24|title=علی کاوه؛ از قیچی تا قیچی|url=https://www.hamshahrionline.ir/news/34804/علی-کاوه-از-قیچی-تا-قیچی|access-date=2020-11-01|website=همشهری آنلاین|language=fa}}</ref><ref>{{Cite web|title=Everyday Projects from Iran|url=http://www.silkroadartgallery.com/6753/everyday-projects-from-iran/|access-date=2020-11-01|language=en-US}}</ref><ref>{{Cite web|date=2015-12-07|title=A Journey Inside - a photo project by 8 Iranian photographers|url=https://www.panos.co.uk/news/a-journey-inside-a-photo-project-by-8-iranian-photographers/|access-date=2020-11-01|website=Panos Pictures|language=en}}</ref><ref>{{Cite web|title=Home|url=https://www.alikaveh.com/|access-date=2020-11-01|website=Ali Kaveh}}</ref>

He started his professional career when he was 15, and covered many international major events during his life. [[World Wrestling Championships|1973 International Wrestling Championship]] in [[Tehran]] was the first International sport event covered by him. Ali Kaveh has also attended [[1974 Asian Games]] in [[Tehran]], [[1976 Summer Olympics|1976 Olympic Games]] in Montreal, [[1990 Asian Games]] in Beijing, [[UEFA Euro 1992|EURO 1992]] in Sweden, [[1996 Summer Olympics|Atlanta 1996 Olympic Games]] and [[1998 FIFA World Cup|FIFA World Cup France 1998]].<ref>{{Cite web|title=HOME|url=http://www.iraninternationalmagazine.com/issue_10/irtp-sydney/text/thirty.htm|access-date=2020-11-01|website=www.iraninternationalmagazine.com}}</ref><ref>{{Cite web|title=روزنامهء هموطن سلام - علی کاوه، عکاس صحنه‌های نشاط و انرژی|url=http://www.hamvatansalam.com/news14801.html|access-date=2020-11-01|website=www.hamvatansalam.com}}</ref><ref>{{Cite web|date=|title=با حضور افشارزاده و نماینده فدراسیون جهانی کشتی انجام شد؛تقدیر فیلا از علی کاوه عکاس ورزشی ایران|url=http://http/%3a%2f%2fwww.olympic.ir%2ffa%2fnews%2f9295%2f%25d8%25a8%25d8%25a7-%25d8%25ad%25d8%25b6%25d9%2588%25d8%25b1-%25d8%25a7%25d9%2581%25d8%25b4%25d8%25a7%25d8%25b1%25d8%25b2%25d8%25a7%25d8%25af%25d9%2587-%25d9%2588-%25d9%2586%25d9%2585%25d8%25a7%25db%258c%25d9%2586%25d8%25af%25d9%2587-%25d9%2581%25d8%25af%25d8%25b1%25d8%25a7%25d8%25b3%25db%258c%25d9%2588%25d9%2586-%25d8%25ac%25d9%2587%25d8%25a7%25d9%2586%25db%258c-%25da%25a9%25d8%25b4%25d8%25aa%25db%258c-%25d8%25a7%25d9%2586%25d8%25ac%25d8%25a7%25d9%2585-%25d8%25b4%25d8%25af-%25d8%25aa%25d9%2582%25d8%25af%25db%258c%25d8%25b1-%25d9%2581%25db%258c%25d9%2584%25d8%25a7-%25d8%25a7%25d8%25b2-%25d8%25b9%25d9%2584%25db%258c-%25da%25a9%25d8%25a7%25d9%2588%25d9%2587-%25d8%25b9%25da%25a9%25d8%25a7%25d8%25b3-%25d9%2588%25d8%25b1%25d8%25b2%25d8%25b4%25db%258c-%25d8%25a7%25db%258c%25d8%25b1%25d8%25a7%25d9%2586|url-status=live|archive-url=|archive-date=|access-date=2020-11-01|website=کمیته ملی المپیک جمهوری اسلامی ایران|language=fa}}</ref>

He is famous in Iran because he took the picture of [[Ruhollah Khomeini|Ayatollah Khomeini]], which printed on Iranian [[10,000 rials note]].<ref>{{Cite web|title=همه چیز دربارۀ "علی کاوه" +عکس - آخرین اخبار - اخبار - میان خبر- پایگاه خبری تحلیلی شهرستان میاندرود|url=http://www.miankhabar.ir/news/3792/%D9%87%D9%85%D9%87-%DA%86%DB%8C%D8%B2-%D8%AF%D8%B1%D8%A8%D8%A7%D8%B1%DB%80-%C2%AB%D8%B9%D9%84%DB%8C-%DA%A9%D8%A7%D9%88%D9%87%C2%BB-+%D8%B9%DA%A9%D8%B3.html|access-date=2020-11-01|website=www.miankhabar.ir}}</ref><ref>{{Cite web|title=Photos of Imam Khomeini Taken by Kalari, Sayyad and Sorayya Ejlal to Be Exhibited|url=http://navideshahed.com/en/news/51010/photos-of-imam-khomeini-taken-by-kalari-sayyad-and-sorayya-ejlal-to-be-exhibited|access-date=2020-11-01|website=navideshahed.com}}</ref><ref>{{Cite web|date=2007-10-10|title=نمایشگاه عکس علی کاوه در گالری مهر|url=https://www.hamshahrionline.ir/news/33637/نمایشگاه-عکس-علی-کاوه-در-گالری-مهر|access-date=2020-11-01|website=همشهری آنلاین|language=fa}}</ref>

== Major Events Covered ==

* 1973 International Wrestling Championship
* Asian Games 1974 Tehran
* 1976 Summer Olympic Games Montreal
* 1990 Asian Games Beijing
* UEFA EURO 1992 Sweden
* 1996 Summer Olympic Games Atlanta
* 1998 FIFA World Cup France

== References ==
<references />

{{DEFAULTSORT:Kaveh, Ali}}
[[Category:Living people]]
[[Category:1946 births]]
[[Category:People from Amol]]
[[Category:Iranian photographers]]
[[Category:Iranian photojournalists]]
""")
ref_tags = wikiCode.filter_tags(matches=lambda node: node.tag == "ref" )
for tag in ref_tags:
    template = tag.contents.filter_templates()[0]
    for param in template.params:
        if re.search( r"url\s*\=", str(param), re.MULTILINE):

            # If URL contains space then it will be invaild url for sure.
            if " " in str(param.value).strip():
                template.remove(param)
                break

            if isValidURL( str(param.value) ) is None:
                if isValidURL( "//" + str(param.value) ):
                    param.value = "//" + str(param.value)
                else:
                    template.remove(param)
print( wikiCode )