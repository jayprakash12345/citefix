from functools import wraps
from uuid import uuid4
from flask import Flask, abort, jsonify, redirect, request, session
import os
import mwoauth
from requests_oauthlib import OAuth1
from flask_cors import CORS


from utils.fixes import Fixes
from utils.api import WikiAPI
from utils.wiki import WikiPage

from utils.helper import _str

app = Flask(__name__)

cors = CORS(app)

# Setting configuration in app
app.config["SECRET_KEY"] = os.urandom(24)

# Loading configuration from external
app.config.from_object("config.production")

consumer_token = mwoauth.ConsumerToken(
    app.config["CONSUMER_KEY"], app.config["CONSUMER_SECRET"]
)
handshaker = mwoauth.Handshaker(
    app.config["OAUTH_MWURI"], consumer_token, user_agent="CiteFix tool"
)
API_URL = app.config["OAUTH_MWURI"] + "api.php"

# This will be replace by Celery and DB
TEMP_DATA = {}

def authorize(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        user = get_current_user()
        if (user == None) or (user == ""):
            abort(401)

        return f(*args, **kws)

    return decorated_function


@app.route("/")
def index():
    return "CiteFix - Backend"


@app.route("/fix", methods=["POST"])
@authorize
def fix():
    data = request.json
    lang = data.get("language", "")
    category = data.get("category", "")
    page = data.get("page", "")

    if "" in [ lang, category, page ]:
        return jsonify(message="Data is missing"), 400

    apiObj = WikiAPI()
    pageObj = WikiPage(page)
    pageObj.setup(apiObj)

    fixes = Fixes(pageObj, apiObj)
    newContent = fixes.apply(category)

    id = str(uuid4()).replace("-", "")
    TEMP_DATA[id] = {
        "metadata": {
            "language": lang,
            "page": page
        },
        "revision": str(newContent)
    }

    return jsonify(id=id), 200



@app.route("/newcontent/<string:uuid>", methods=["POST"])
@authorize
def newContent(uuid):
    if (uuid is not None) and (uuid != ""):
        if uuid in TEMP_DATA.keys():
            return jsonify(TEMP_DATA[uuid]), 200

    return jsonify(message="No data for this id"), 400


#############################
# ------------ User ---------
#############################
@app.route("/profile")
def api_profile():
    return jsonify(
        {"logged": get_current_user() is not None, "username": get_current_user()}
    )


#############################
# ------------ Login --------
#############################
@app.route("/login")
def login():
    redirect_to, request_token = handshaker.initiate()
    keyed_token_name = _str(request_token.key) + "_request_token"
    keyed_next_name = _str(request_token.key) + "_next"
    session[keyed_token_name] = dict(zip(request_token._fields, request_token))
    if "next" in request.args:
        session[keyed_next_name] = request.args.get("next")
    else:
        session[keyed_next_name] = "index"
    return redirect(redirect_to)


@app.route("/logout")
def logout():
    session["mwoauth_access_token"] = None
    session["mwoauth_username"] = None
    return redirect("https://citefix.wmcloud.org")


@app.route("/oauth-callback")
def oauth_callback():
    request_token_key = request.args.get("oauth_token", "None")
    keyed_token_name = _str(request_token_key) + "_request_token"
    keyed_next_name = _str(request_token_key) + "_next"
    if keyed_token_name not in session:
        err_msg = "OAuth callback failed. Can't find keyed token. Are cookies disabled?"
        err_msg = err_msg + '\n Go <a href="https://citefix.wmcloud.org">CiteFix</a>'
        return jsonify(message=err_msg)
    access_token = handshaker.complete(
        mwoauth.RequestToken(**session[keyed_token_name]), request.query_string
    )
    session["mwoauth_access_token"] = dict(zip(access_token._fields, access_token))
    del session[keyed_next_name]
    del session[keyed_token_name]
    get_current_user(False)
    return redirect("https://citefix.wmcloud.org")


def get_current_user(cached=True):
    if cached:
        return session.get("mwoauth_username")
    # Get user info
    identity = handshaker.identify(
        mwoauth.AccessToken(**session["mwoauth_access_token"])
    )
    # Store user info in session
    session["mwoauth_username"] = identity["username"]
    return session["mwoauth_username"]


def authenticated_session():
    if "mwoauth_access_token" in session:
        auth = OAuth1(
            client_key=app.config["CONSUMER_KEY"],
            client_secret=app.config["CONSUMER_SECRET"],
            resource_owner_key=session["mwoauth_access_token"]["key"],
            resource_owner_secret=session["mwoauth_access_token"]["secret"],
        )
        return auth
    return None


if __name__ == "__main__":
    app.run()
