class WikiPage:
	page: str = ""
	content: str = ""
	categories: list = []

	def __init__(self, page) -> None:
		self.page = page
	
	def setup(self, apiObj) -> None:
		self.getPageContent(apiObj)
		self.getPageCategories(apiObj)

	def getPageName(self) -> str:
		return self.page

	def getPageContent(self, apiObj) -> str:
		if self.content == "":
			self.content = apiObj.getPageContent( self.page )

		return self.content
	
	def getPageCategories(self, apiObj) -> list:
		if len(self.categories) == 0:
			self.categories = apiObj.getPageCategories( self.page )

		return self.categories