from urllib.parse import urlparse


def isValidURL( url: str):
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False