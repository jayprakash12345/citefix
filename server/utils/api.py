from typing import Union
import requests

class WikiAPI:
    lang: str = "en"
    project: str = "wikipedia"
    session: requests.Session = None

    def __init__(self) -> None:
        self.session = requests.Session()
        self.session.headers.update({
            "Accept-Encoding": "gzip",
            "User-Agent": "CiteFix/1.0 (0freerunning@gmail.org) requests/2.28.2"
        })

    def getApiUrl(self) -> str:
        return f"https://{self.lang}.{self.project}.org/w/api.php"

    def getPageContent(self, page: str ) -> Union[str, None]:
        params = {
            "action": "parse",
            "format": "json",
            "page": page,
            "prop": "wikitext",
            "formatversion": "2"
        }

        try:
            resp = self.session.get( self.getApiUrl(), params=params)
            if resp.status_code == 200:
                data = resp.json()
                return data["parse"]["wikitext"]
            else:
                return None
        except:
            return None
    
    def getPageCategories(self, page) -> Union[list, None]:
        params = {
            "action": "query",
            "format": "json",
            "titles": page,
            "prop": "categories",
            "cllimit": "max",
            "formatversion": "2"
        }

        try:
            resp = self.session.get( self.getApiUrl(), params=params)
            if resp.status_code == 200:
                data = resp.json()
                return [ i["title"] for i in data["query"]["pages"][0]["categories"] ]
            else:
                return None
        except:
            return None