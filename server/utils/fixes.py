import re
from mwparserfromhell import parse
from utils.url import isValidURL

class Fixes:
    pageObj = None
    parseObj = None
    refTags = None

    def __init__(self, pageObj, apiObj) -> None:
        self.pageObj = pageObj
        self.parseObj = parse(pageObj.getPageContent(apiObj))
        self.refTags = self.parseObj.filter_tags(matches=lambda node: node.tag == "ref")

    def apply(self, cat):
        if cat == "cs1":
            self.fixCS1()

        return self.parseObj

    def fixCS1(self):
        for tag in self.refTags:
            templates = tag.contents.filter_templates()

            if len(templates) <= 0:
                continue

            # Moslty ref contains only one template like Cite
            template = templates[0]

            for param in template.params:
                if re.search(r"url\s*\=", str(param), re.MULTILINE):
                    # If URL contains space then it will be invaild url for sure.
                    if " " in str(param.value).strip():
                        template.remove(param)
                        break

                    if isValidURL(str(param.value)) is None:
                        if isValidURL("//" + str(param.value)):
                            param.value = "//" + str(param.value)
                        else:
                            template.remove(param)
